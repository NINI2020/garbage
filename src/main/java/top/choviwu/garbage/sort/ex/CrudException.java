package top.choviwu.garbage.sort.ex;

import lombok.Data;

import java.util.function.Supplier;

@Data
public class CrudException extends RuntimeException {


    private String msg;

    public CrudException(String msg){
        super(msg);
        this.msg = msg;
    }
}
