package top.choviwu.garbage.sort.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import top.choviwu.garbage.sort.constant.GsCode;

@Data
@Accessors(chain = true)
public class GsInfo {

    private String name;
    private GsCode type;
//    private
}
