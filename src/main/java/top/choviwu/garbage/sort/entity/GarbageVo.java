package top.choviwu.garbage.sort.entity;

import lombok.Data;
import top.choviwu.garbage.sort.constant.GsCode;

import java.time.LocalDateTime;
@Data
public class GarbageVo {


    private static final long serialVersionUID=1L;

    private Integer id;

    private String gName;

    private GsCode type;

    private LocalDateTime gTime;

}
