package top.choviwu.garbage.sort.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
@Data
@EqualsAndHashCode
@ToString
public class User implements Serializable {

    private static final long serialVersionUID=1L;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String userName;

    private Integer age;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String ip;

    public User(String usn,Integer age){
        this.userName = usn;
        this.age = age;
    }


}
