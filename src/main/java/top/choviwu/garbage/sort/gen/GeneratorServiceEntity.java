package top.choviwu.garbage.sort.gen;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.HashMap;
import java.util.Map;

public class GeneratorServiceEntity {

    public static void main(String[] args) {
        new GeneratorServiceEntity().generateCode();
    }

        public void generateCode() {
            String packageName = "org.choviwu.shezhen.garbage.sort";
            boolean serviceNameStartWithI = false;//user -> UserService, 设置成true: user -> IUserService
            generateByTables(serviceNameStartWithI, packageName, "user");
        }

        private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
            GlobalConfig config = new GlobalConfig();
            String dbUrl = "jdbc:mysql://localhost:3306/shenzhen?useUnicode=true&characterEncoding=utf8&autoReconnect=true";
            DataSourceConfig dataSourceConfig = new DataSourceConfig();
            dataSourceConfig.setDbType(DbType.MYSQL)
                    .setUrl(dbUrl)
                    .setUsername("root")
                    .setPassword("root")
                    .setDriverName("com.mysql.jdbc.Driver");
            StrategyConfig strategyConfig = new StrategyConfig();
            strategyConfig
                    .setCapitalMode(true)
                    .setEntityLombokModel(false)
                    //.setDbColumnUnderline(true)
                    .setNaming(NamingStrategy.underline_to_camel)
                    .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
            config.setActiveRecord(false)
                    .setAuthor("奕仁")
                    .setOutputDir("D:/")
                    .setFileOverride(true);
            if (!serviceNameStartWithI) {
                config.setServiceName("%sService");
            }
            // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
            InjectionConfig cfg = new InjectionConfig() {
                @Override
                public void initMap() {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("奕仁", this.getConfig().getGlobalConfig().getAuthor() );
                    this.setMap(map);
                }
            };
            new AutoGenerator().setGlobalConfig(config)
                    .setDataSource(dataSourceConfig)
                    .setStrategy(strategyConfig)
                    .setCfg(cfg)
                    .setPackageInfo(
                            new PackageConfig()
                                    .setParent(packageName)
                                    .setController("controller")
                                    .setEntity("entity")
                    ).execute();
        }

        private void generateByTables(String packageName, String... tableNames) {
            generateByTables(true, packageName, tableNames);
        }
}
