package top.choviwu.garbage.sort.controller;


import top.choviwu.garbage.sort.service.UserService;
import top.choviwu.garbage.sort.util.CusAccessObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
@RestController
@RequestMapping("/user")
public class UserController {



    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public Object register(HttpServletRequest request){
        String ip = CusAccessObjectUtil.getIpAddress(request);

        return userService.register(ip);
    }

}

