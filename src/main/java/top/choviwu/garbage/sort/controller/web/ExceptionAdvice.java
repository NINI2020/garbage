package top.choviwu.garbage.sort.controller.web;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import top.choviwu.garbage.sort.ex.CrudException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@ControllerAdvice
@EnableWebMvc
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object exception(HttpServletResponse response,Exception e) {
        e.printStackTrace();
        return ApiResponse.error("服务器异常,稍后再试");
    }

    @ResponseBody
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object exception(MaxUploadSizeExceededException e) {
//        return "文件上传过大，请上传10M以内的图片";
        return ApiResponse.error("文件上传过大,请上传10M以内的图片");
    }

    @ResponseBody
    @ExceptionHandler(CrudException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object exception(CrudException e, HttpServletResponse response) {
        return ApiResponse.error(e.getMsg());

    }

    @ResponseBody
    @ExceptionHandler(ServletException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object exception(ServletException e) {
        return ApiResponse.error("服务器异常,稍后再试");
    }

}
