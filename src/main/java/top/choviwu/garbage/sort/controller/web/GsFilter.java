package top.choviwu.garbage.sort.controller.web;

import top.choviwu.garbage.sort.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Component
@WebFilter("gsFilter")
public class GsFilter implements Filter {

    @Autowired
    private UserMapper userMapper;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//
//        String ip = CusAccessObjectUtil.getIpAddress(request);
//        User user = userMapper.selectOne(new QueryWrapper<User>().eq(true,"ip",ip));
//        if(null ==user){
//            throw new CrudException("请注册账号");
//        }
//
//        Map<String,String[]> map = request.getParameterMap();
//
//        CollUtil.sort(map,(o1,o2)->o1.compareTo(o2));
//        StringBuilder sb = new StringBuilder();
//        Iterator<Map.Entry<String,String[]>> iterator = map.entrySet().iterator();
        ((HttpServletResponse)servletResponse).setHeader("cors","*");
        ((HttpServletResponse)servletResponse).setHeader("token", UUID.randomUUID().toString().replace("-",""));
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
