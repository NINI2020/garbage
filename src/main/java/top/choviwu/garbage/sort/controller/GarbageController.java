package top.choviwu.garbage.sort.controller;


import cn.hutool.core.collection.CollUtil;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import top.choviwu.garbage.sort.annotation.GLog;
import top.choviwu.garbage.sort.annotation.Limit;
import top.choviwu.garbage.sort.annotation.LogType;
import top.choviwu.garbage.sort.service.GarbageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
@Controller
@RequestMapping("/garbage")
public class GarbageController {

    @Autowired
    private GarbageService garbageService;

    @Autowired
    Environment environment;


    @Limit
    @GLog(LogType.upload)
    @ResponseBody
    @RequestMapping(value = "/uploadFile")
    public Object getAiList(@RequestParam(value = "file") MultipartFile multipartFile){

        String path = environment.getProperty("user.dir","/root/garbage/upload");
        File file = new File(path+"/"+ multipartFile.getOriginalFilename());

        try {
            multipartFile.transferTo(file);
        //106.13.194.203
            return garbageService.aiImage(file.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
            return CollUtil.newArrayList();
        }

    }

    @Limit
    @GLog
    @ResponseBody
    @RequestMapping(value = "/getGarbage",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object getGarbage(String garbageName){
        Object list =  garbageService.dealGarbage(garbageName);

        return list;
    }
    @GLog(value = LogType.group)
    @ResponseBody
    @RequestMapping(value = "/countGroup",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Map> countGroup(){
        return garbageService.countGroup();

    }

    @GLog(value = LogType.recent)
    @ResponseBody
    @RequestMapping(value = "/recentGs",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Map> recentGs(){
        return garbageService.recentGs();

    }
}

