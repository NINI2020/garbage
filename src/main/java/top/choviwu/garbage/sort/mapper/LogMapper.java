package top.choviwu.garbage.sort.mapper;

import top.choviwu.garbage.sort.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
public interface LogMapper extends BaseMapper<Log> {

}
