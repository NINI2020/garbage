package top.choviwu.garbage.sort.mapper;

import top.choviwu.garbage.sort.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
public interface UserMapper extends BaseMapper<User> {

}
