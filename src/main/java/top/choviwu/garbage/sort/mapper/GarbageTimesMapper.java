package top.choviwu.garbage.sort.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.choviwu.garbage.sort.entity.GarbageTimes;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2019-08-22
 */
public interface GarbageTimesMapper extends BaseMapper<GarbageTimes> {

}
