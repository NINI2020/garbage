package top.choviwu.garbage.sort.mapper;

import top.choviwu.garbage.sort.entity.Garbage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
public interface GarbageMapper extends BaseMapper<Garbage> {

    List<Map> countGroup();


    List<Map> recentGs();
}
