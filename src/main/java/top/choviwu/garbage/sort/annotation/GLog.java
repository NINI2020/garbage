package top.choviwu.garbage.sort.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GLog {

    LogType value() default LogType.search;
}
