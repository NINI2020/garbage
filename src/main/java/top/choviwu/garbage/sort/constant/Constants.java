package top.choviwu.garbage.sort.constant;

public class Constants {




    public static final String RESULT = "result";
    public static final String KEYWORD = "keyword";
    public static final String NONE = "暂无";

    /**
    * @Description: 百度
    * @author choviwu
    */

    public static final String BAIDU_APPID = "baidu_appid";
    public static final String BAIDU_APIKEP = "baidu_apikey";
    public static final String BAIDU_APPSECRET = "baidu_secretkey";
    public static final String BAIDU_NUM = "baike_num";

    public static final int CONNECT_TIMEOUT_MILLIS_SECOND = 2000;
    public static final int SOCKET_TIMEOUT_MILLIS_SECOND = 60000;
    public static  final Long BASE_SECOND = 1000L;


    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String SEVEN = "7";
    public static final String EIGHT = "8";
    public static final String NINE = "9";
    public static final String TEN = "10";


}
