package top.choviwu.garbage.sort.constant;

import lombok.Getter;
import lombok.Setter;

public interface SmallConstant {



    enum  Tencent{
        /**
         * 对图片进行物体识别，快速找出图片中包含的物体信息
         */
        VISION_OBJECTR("https://api.ai.qq.com/fcgi-bin/vision/vision_objectr"),
        APP_ID(""),
        APP_KEY("")
        ;
        @Getter
                @Setter
        String value;

        Tencent(String value){
            this.value = value;
        }

    }


    enum  Baidu{
        /**
         * 对图片进行物体识别，快速找出图片中包含的物体信息
         */
        VISION_OBJECTR("https://aip.baidubce.com/rest/2.0/image-classify/v2/advanced_general"),
        APP_ID(""),
        API_KEY(""),
        SECRET_KEY(""),
        ;
        @Getter
                @Setter
        String value;

        Baidu(String value){
            this.value = value;
        }

    }
}
