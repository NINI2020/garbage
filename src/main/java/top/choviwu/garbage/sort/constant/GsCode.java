package top.choviwu.garbage.sort.constant;

import lombok.Getter;
import lombok.Setter;

public enum GsCode {
//0 为可回收、1 为有害、2 为厨余(湿)、3 为其他(干)
    KEHUISHOU(0,"可回收"),
    YOUHAI(1,"有害垃圾"),
    SHI(2,"湿垃圾"),
    GAN(3,"干垃圾"),
    OTHER(4,"其他垃圾"),
    UNKNOW(-1,"暂无")


    ;
    @Getter
    @Setter
    private Integer type;

//    @Getter
//    @Setter
//    private String name;

    @Getter
    @Setter
    private String value;
    GsCode(Integer type,String value ){
        this.type = type;
        this.value = value;
    }
}
