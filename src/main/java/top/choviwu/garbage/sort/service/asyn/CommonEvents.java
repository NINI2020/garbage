package top.choviwu.garbage.sort.service.asyn;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommonEvents implements InitializingBean {

    private CommonEvents events;

    public static EventQueue<String> garbageEventHandlerEventQueue;

    @Autowired
    private GarbageAbstractEventHandler garbageEventHandler;


    @Override
    public void afterPropertiesSet() throws Exception {
        events = this;
        /**
        * 创建队列并初始化线程池
        */
        garbageEventHandlerEventQueue = new EventQueue(events.garbageEventHandler,10);

    }
}
