package top.choviwu.garbage.sort.service.impl;

import top.choviwu.garbage.sort.entity.Config;
import top.choviwu.garbage.sort.mapper.ConfigMapper;
import top.choviwu.garbage.sort.service.ConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {


    @Autowired
    private ConfigMapper configMapper;
}
