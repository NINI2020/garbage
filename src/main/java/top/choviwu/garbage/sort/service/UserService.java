package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
public interface UserService extends IService<User> {


    User register(String ip);
}
