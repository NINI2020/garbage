package top.choviwu.garbage.sort.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.choviwu.garbage.sort.entity.GarbageTimes;
import top.choviwu.garbage.sort.mapper.GarbageTimesMapper;
import top.choviwu.garbage.sort.service.GarbageTimesService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-08-22
 */
@Service
public class GarbageTimesServiceImpl extends ServiceImpl<GarbageTimesMapper, GarbageTimes> implements GarbageTimesService {

}
