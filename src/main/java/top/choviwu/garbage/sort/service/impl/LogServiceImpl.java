package top.choviwu.garbage.sort.service.impl;

import top.choviwu.garbage.sort.entity.Log;
import top.choviwu.garbage.sort.mapper.LogMapper;
import top.choviwu.garbage.sort.service.LogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

}
