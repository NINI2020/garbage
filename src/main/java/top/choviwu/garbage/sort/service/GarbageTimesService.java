package top.choviwu.garbage.sort.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.choviwu.garbage.sort.entity.GarbageTimes;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-08-22
 */
public interface GarbageTimesService extends IService<GarbageTimes> {

}
