package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.Garbage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
public interface GarbageService extends IService<Garbage> {
    /**
     * laji
     * @param name
     * @return
     */
    Object dealGarbage(String name);

    /**
     * ai show
     * @param name
     * @return
     */
    List aiImage(String name);

    List countGroup();

    List recentGs();

//    List getHotGarbages();
}
