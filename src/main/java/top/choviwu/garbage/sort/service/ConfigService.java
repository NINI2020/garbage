package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.Config;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
public interface ConfigService extends IService<Config> {

}
