package top.choviwu.garbage.sort.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.choviwu.garbage.sort.entity.Garbage;
import top.choviwu.garbage.sort.entity.GsInfo;
import top.choviwu.garbage.sort.ex.CrudException;
import top.choviwu.garbage.sort.mapper.GarbageMapper;
import top.choviwu.garbage.sort.mapper.GarbageTimesMapper;
import top.choviwu.garbage.sort.service.GarbageService;
import top.choviwu.garbage.sort.service.asyn.CommonEvents;
import top.choviwu.garbage.sort.util.BaiduGsUtils;
import top.choviwu.garbage.sort.util.JsoupUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static top.choviwu.garbage.sort.constant.Constants.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
@Service
@Slf4j
public class GarbageServiceImpl extends ServiceImpl<GarbageMapper, Garbage> implements GarbageService {


    @Autowired
    private BaiduGsUtils baiduGsUtils;

    @Autowired
    private GarbageMapper garbageMapper;
    @Autowired
    GarbageTimesMapper garbageTimesMapper;

    @Override
    public Object dealGarbage(String name) {

        List<Garbage> list = garbageMapper.selectList(new QueryWrapper().eq(true, "g_name", name));
        if (list.isEmpty()) {
            GsInfo[] codes = JsoupUtils.getGsList(name);

            for (GsInfo code : codes) {
                if (!garbageMapper.selectList(new QueryWrapper<Garbage>()
                        .eq(true, "g_name", code.getName())).isEmpty()) {
                    continue;
                }
                Garbage garbage = new Garbage();
                garbage.setGName(code.getName());
                garbage.setGTime(LocalDateTime.now());
                garbage.setGType(code.getType().getValue());
                garbageMapper.insert(garbage);

                list.add(garbage);
            }
        }
        if (list.isEmpty()) {
            throw new CrudException(NONE);
        }
        return list;
    }

    @Override
    public List aiImage(String path) {
        JSONObject jsonObject = baiduGsUtils.aiImage(path);
        List list = CollUtil.newArrayList();
        try {
            JSONArray array = jsonObject.getJSONArray(RESULT);
            JSONObject[] objects = new JSONObject[array.length()];
            for (int i = 0; i < array.length(); i++) {
                objects[i] = array.getJSONObject(i);
            }
            Arrays.asList(objects).stream().map(c -> {
                try {
                    list.add(c.getString(KEYWORD));
                    return c.getString(KEYWORD);
                } catch (JSONException e) {
                    return "";
                }
            }).forEach(c -> Optional.ofNullable(c).ifPresent(this::queueOffer));
        } catch (JSONException e) {
            log.error(">>>>>>>>>.Exception {}", e);
            list.add(NONE);
        }
        return list;
    }

    @Override
    public List<Map> countGroup() {
        return garbageMapper.countGroup();

    }
    @Override
    public List<Map> recentGs() {
        return garbageMapper.recentGs();

    }

//    @Override
//    public List getHotGarbages() {
//        return garbageTimesMapper.selectList(Wrappers.query().orderByDesc("times"))
//
//    }

    private void queueOffer(String keyWord) {
        log.info(">>>>>>>>>.关键字 {}", keyWord);
        CommonEvents.garbageEventHandlerEventQueue.offer(keyWord);
    }


}
