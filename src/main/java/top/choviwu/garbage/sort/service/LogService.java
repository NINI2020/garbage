package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.Log;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
public interface LogService extends IService<Log> {

}
