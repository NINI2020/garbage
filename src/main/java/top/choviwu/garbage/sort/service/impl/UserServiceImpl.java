package top.choviwu.garbage.sort.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import top.choviwu.garbage.sort.entity.User;
import top.choviwu.garbage.sort.mapper.UserMapper;
import top.choviwu.garbage.sort.redis.RedisEnum;
import top.choviwu.garbage.sort.redis.RedisRepository;
import top.choviwu.garbage.sort.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    RedisRepository redisRepository;
    @Override
    public User register(String ip) {

        User user = this.getOne(new QueryWrapper<User>().eq(true,"ip",ip));
        if(null !=user){
            return user;
        }
        String appId = RandomUtil.randomString(10);
        String appKey = RandomUtil.randomString(16);

//        user = new User();
//        user.setAppId(appId);
//        user.setAppKey(appKey);
//        this.save(userName);
        return user;
    }
}
