package top.choviwu.garbage.sort.service.asyn;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.choviwu.garbage.sort.service.GarbageService;

import static top.choviwu.garbage.sort.constant.Constants.BASE_SECOND;

@Component
@Slf4j
public class GarbageAbstractEventHandler extends AbstractEventHandler<String> {

    @Autowired(required = false)
    private GarbageService garbageService;

    @Override
    public void handle(String event) {
        log.info("start >>>" + System.currentTimeMillis() / BASE_SECOND);
        try {
            garbageService.dealGarbage(event);
        } finally {
            log.info("end>>>>>" + System.currentTimeMillis() / BASE_SECOND);
        }

    }
}
