/*
 Navicat Premium Data Transfer

 Source Server         : choviwu
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : shenzhen

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 21/07/2019 11:18:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `param` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for garbage
-- ----------------------------
DROP TABLE IF EXISTS `garbage`;
CREATE TABLE `garbage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `g_type` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `g_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `garbage_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `times` int(5) DEFAULT '0',
  `garbage_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `open_id` varchar(255) COLLATE utf8mb4_bin DEFAULT '0',
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `host` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `referer` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `agent` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `response` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `app_key` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_idx` (`ip`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

SET FOREIGN_KEY_CHECKS = 1;
